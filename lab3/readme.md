```python
import matplotlib.pyplot as plt
import numpy as np
import pandas as pd
```


```python
#zakres osi x - określenie przyjmowanych wartości
x = np.linspace(0, 5, 100)
```


```python
#wykres funkcji liniowej
plt.plot(x, x, label='Wykres funkcji liniowej')
plt.xlabel('x')
plt.ylabel('y')
plt.title("Funkcja liniowa")
```




    Text(0.5, 1.0, 'Funkcja liniowa')




![png](output_2_1.png)



```python
#wykres funkcji kwadratowej
plt.plot(x, x**2, label='Wykres funkcji kwadratowej')
plt.xlabel('x')
plt.ylabel('y')
plt.title("Funkcja kwadratowa")
```




    Text(0.5, 1.0, 'Funkcja kwadratowa')




![png](output_3_1.png)



```python
#wykres funkcji sześciennej
plt.plot(x, x**3, label='Wykres funkcji sześciennej')
plt.xlabel('x')
plt.ylabel('y')
plt.title("Funkcja sześcienna")
```




    Text(0.5, 1.0, 'Funkcja sześcienna')




![png](output_4_1.png)



```python
#wykres funkcji cosinus
x = np.arange(0, 10, 0.2)
y = np.cos(x)
plt.plot(x,y)
plt.xlabel('x')
plt.ylabel('y')
plt.title("wykres cos")
```




    Text(0.5, 1.0, 'wykres cos')




![png](output_5_1.png)



```python
#import pliku z ogłoszeniami
auta = pd.read_csv("samochody1tys.csv")
```


```python
auta
```




<div>
<style scoped>
    .dataframe tbody tr th:only-of-type {
        vertical-align: middle;
    }

    .dataframe tbody tr th {
        vertical-align: top;
    }

    .dataframe thead th {
        text-align: right;
    }
</style>
<table border="1" class="dataframe">
  <thead>
    <tr style="text-align: right;">
      <th></th>
      <th>id</th>
      <th>marka</th>
      <th>model</th>
      <th>rok_produkcji</th>
      <th>rodzaj_silnika</th>
      <th>pojemnosc_silnika</th>
      <th>przebieg</th>
      <th>cena</th>
      <th>wojewodztwo</th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <th>0</th>
      <td>0</td>
      <td>Nissan</td>
      <td>X-Trail III</td>
      <td>2014.0</td>
      <td>Diesel</td>
      <td>1600.0</td>
      <td>160000.0</td>
      <td>75900.0</td>
      <td>Dolnośląskie</td>
    </tr>
    <tr>
      <th>1</th>
      <td>1</td>
      <td>BMW</td>
      <td>X1 E84</td>
      <td>2012.0</td>
      <td>Diesel</td>
      <td>2000.0</td>
      <td>149000.0</td>
      <td>70900.0</td>
      <td>Świętokrzyskie</td>
    </tr>
    <tr>
      <th>2</th>
      <td>2</td>
      <td>Opel</td>
      <td>Zafira A</td>
      <td>2004.0</td>
      <td>Benzyna+CNG</td>
      <td>1600.0</td>
      <td>99000.0</td>
      <td>9800.0</td>
      <td>Opolskie</td>
    </tr>
    <tr>
      <th>3</th>
      <td>3</td>
      <td>Hyundai</td>
      <td>i10 I</td>
      <td>2008.0</td>
      <td>Benzyna</td>
      <td>1100.0</td>
      <td>93000.0</td>
      <td>11900.0</td>
      <td>Łódzkie</td>
    </tr>
    <tr>
      <th>4</th>
      <td>4</td>
      <td>Volkswagen</td>
      <td>CC</td>
      <td>2010.0</td>
      <td>Diesel</td>
      <td>1968.0</td>
      <td>127428.0</td>
      <td>49900.0</td>
      <td>Wielkopolskie</td>
    </tr>
    <tr>
      <th>...</th>
      <td>...</td>
      <td>...</td>
      <td>...</td>
      <td>...</td>
      <td>...</td>
      <td>...</td>
      <td>...</td>
      <td>...</td>
      <td>...</td>
    </tr>
    <tr>
      <th>995</th>
      <td>995</td>
      <td>Alfa Romeo</td>
      <td>159</td>
      <td>2009.0</td>
      <td>Diesel</td>
      <td>2387.0</td>
      <td>182000.0</td>
      <td>33900.0</td>
      <td>Małopolskie</td>
    </tr>
    <tr>
      <th>996</th>
      <td>996</td>
      <td>Škoda</td>
      <td>Fabia II</td>
      <td>2012.0</td>
      <td>Benzyna</td>
      <td>1200.0</td>
      <td>42700.0</td>
      <td>25400.0</td>
      <td>Lubelskie</td>
    </tr>
    <tr>
      <th>997</th>
      <td>997</td>
      <td>Renault</td>
      <td>Modus</td>
      <td>2004.0</td>
      <td>Benzyna</td>
      <td>1200.0</td>
      <td>168111.0</td>
      <td>10900.0</td>
      <td>Łódzkie</td>
    </tr>
    <tr>
      <th>998</th>
      <td>998</td>
      <td>Audi</td>
      <td>A3 8L</td>
      <td>2000.0</td>
      <td>Benzyna</td>
      <td>1595.0</td>
      <td>226500.0</td>
      <td>5400.0</td>
      <td>Małopolskie</td>
    </tr>
    <tr>
      <th>999</th>
      <td>999</td>
      <td>Volvo</td>
      <td>XC 60 I</td>
      <td>2016.0</td>
      <td>Diesel</td>
      <td>1969.0</td>
      <td>13617.0</td>
      <td>136900.0</td>
      <td>Małopolskie</td>
    </tr>
  </tbody>
</table>
<p>1000 rows × 9 columns</p>
</div>




```python
#wydobycie marek aut z tabeli
var1 = auta[["marka"]]
```


```python
var1
```




<div>
<style scoped>
    .dataframe tbody tr th:only-of-type {
        vertical-align: middle;
    }

    .dataframe tbody tr th {
        vertical-align: top;
    }

    .dataframe thead th {
        text-align: right;
    }
</style>
<table border="1" class="dataframe">
  <thead>
    <tr style="text-align: right;">
      <th></th>
      <th>marka</th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <th>0</th>
      <td>Nissan</td>
    </tr>
    <tr>
      <th>1</th>
      <td>BMW</td>
    </tr>
    <tr>
      <th>2</th>
      <td>Opel</td>
    </tr>
    <tr>
      <th>3</th>
      <td>Hyundai</td>
    </tr>
    <tr>
      <th>4</th>
      <td>Volkswagen</td>
    </tr>
    <tr>
      <th>...</th>
      <td>...</td>
    </tr>
    <tr>
      <th>995</th>
      <td>Alfa Romeo</td>
    </tr>
    <tr>
      <th>996</th>
      <td>Škoda</td>
    </tr>
    <tr>
      <th>997</th>
      <td>Renault</td>
    </tr>
    <tr>
      <th>998</th>
      <td>Audi</td>
    </tr>
    <tr>
      <th>999</th>
      <td>Volvo</td>
    </tr>
  </tbody>
</table>
<p>1000 rows × 1 columns</p>
</div>




```python
#wyliczenie liczb brandów pojazdów
index = pd.Index(var1["marka"])
data = index.value_counts()
data
```




    Opel             105
    Volkswagen        92
    Audi              70
    BMW               66
    Renault           59
    Ford              59
    Mercedes-Benz     59
    Toyota            49
    Peugeot           41
    Citroën           40
    Škoda             36
    Fiat              33
    Nissan            33
    Hyundai           31
    Kia               25
    Honda             25
    Volvo             24
    Mazda             21
    Seat              20
    Mitsubishi        18
    Suzuki            16
    Chevrolet         11
    Mini              11
    Lexus              7
    Jeep               6
    Alfa Romeo         5
    Dacia              5
    Land Rover         5
    Dodge              5
    SsangYong          3
    Saab               3
    Jaguar             2
    Smart              2
    Daewoo             2
    Infiniti           2
    Porsche            2
    Chrysler           1
    Subaru             1
    Ligier             1
    Pontiac            1
    Lancia             1
    Maserati           1
    Daihatsu           1
    Name: marka, dtype: int64




```python
#przypisanie kolumn do osobnych tablic
liczba = data.values
nazwa = data.index.values
```


```python
#wykres występowania danych marek
plt.bar(nazwa, liczba)
plt.xlabel("marka")
plt.ylabel("ilość aut")
plt.title("wykres prezentujący ilość samochodów poszczególnych marek")
```




    Text(0.5, 1.0, 'wykres prezentujący ilość samochodów poszczególnych marek')




![png](output_12_1.png)



```python
#pobieranie kolumny z województwami
var2 = auta[['wojewodztwo']]
```


```python
#przypisanie danych województw
indexw = pd.Index(var2["wojewodztwo"])
dataw = indexw.value_counts()
```


```python
#przypisanie kolumn do osobnych tablic
liczbaw = dataw.values
nazwaw = dataw.index.values
```


```python
#utworzenie wykresu liczby występowania aut z poszczególnych województw
explode = (0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0)
plt.pie(liczbaw, explode=explode, labels=nazwaw, autopct='%1.1f%%',shadow=False, startangle=0)
plt.axis('equal')
plt.title("Diagram kołowy ogłoszeń z województw")
```




    Text(0.5, 1.0, 'Diagram kołowy ogłoszeń z województw')




![png](output_16_1.png)



```python
#pobranie kolumny roku produkcji
var3 = auta[['rok_produkcji']]
```


```python
indexr = pd.Index(var3["rok_produkcji"])
datar = indexr.value_counts()
```


```python
#histogram utworzony na podstawie lat produkcji pojazdów
plt.hist(datar, density=True, bins=31)
plt.show()
```


![png](output_19_0.png)



```python
datar
```




    2017.0    132
    2007.0     81
    2016.0     77
    2006.0     76
    2009.0     63
    2008.0     59
    2005.0     56
    2014.0     51
    2004.0     48
    2010.0     48
    2003.0     42
    2012.0     39
    2011.0     39
    2002.0     36
    2015.0     32
    2013.0     32
    2001.0     29
    2000.0     19
    1999.0     10
    1998.0      7
    1997.0      5
    1994.0      4
    1995.0      3
    1992.0      3
    1984.0      2
    1986.0      2
    1979.0      1
    1993.0      1
    1991.0      1
    1988.0      1
    1996.0      1
    Name: rok_produkcji, dtype: int64




```python
var3
```




<div>
<style scoped>
    .dataframe tbody tr th:only-of-type {
        vertical-align: middle;
    }

    .dataframe tbody tr th {
        vertical-align: top;
    }

    .dataframe thead th {
        text-align: right;
    }
</style>
<table border="1" class="dataframe">
  <thead>
    <tr style="text-align: right;">
      <th></th>
      <th>rok_produkcji</th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <th>0</th>
      <td>2014.0</td>
    </tr>
    <tr>
      <th>1</th>
      <td>2012.0</td>
    </tr>
    <tr>
      <th>2</th>
      <td>2004.0</td>
    </tr>
    <tr>
      <th>3</th>
      <td>2008.0</td>
    </tr>
    <tr>
      <th>4</th>
      <td>2010.0</td>
    </tr>
    <tr>
      <th>...</th>
      <td>...</td>
    </tr>
    <tr>
      <th>995</th>
      <td>2009.0</td>
    </tr>
    <tr>
      <th>996</th>
      <td>2012.0</td>
    </tr>
    <tr>
      <th>997</th>
      <td>2004.0</td>
    </tr>
    <tr>
      <th>998</th>
      <td>2000.0</td>
    </tr>
    <tr>
      <th>999</th>
      <td>2016.0</td>
    </tr>
  </tbody>
</table>
<p>1000 rows × 1 columns</p>
</div>




```python

```


```python

```
